#  Template configuration file for britney

## general input
# Paths to suites in a local mirror
# Note: Britney will only need metadata files and not the debs/dscs
# themselves.
UNSTABLE          = /path/to/source/suite
TESTING           = /path/to/target/suite
# Items in these two (optional) suites will require manual approval
#TPU               = /path/to/secondary-source/suite
#PU                = /path/to/another-source/suite

# By default, package removal propagates to the target suite. To disable this,
# e.g. for partial suites like experimental or spu, uncomment the next line
#PARTIAL_SOURCE    = true

# Output
NONINST_STATUS      = /path/to/britneys-output-dir/non-installable-status
EXCUSES_OUTPUT      = /path/to/britneys-output-dir/excuses.html
EXCUSES_YAML_OUTPUT = /path/to/britneys-output-dir/excuses.yaml
UPGRADE_OUTPUT      = /path/to/britneys-output-dir/output.txt
HEIDI_OUTPUT        = /path/to/britneys-output-dir/HeidiResult
HEIDI_DELTA_OUTPUT  = /path/to/britneys-output-dir/HeidiResultDelta

# External policy/constraints/faux-packages information that
# (presumably) rarely changes.  Examples include "constraints".
#STATIC_INPUT_DIR = /path/to/static/input

# Directory for input files that Britney will update herself
# (e.g. aging information) or will need regular updates
# (e.g. urgency information).
STATE_DIR          = /path/to/britey/state-dir

# List of architectures that Britney should consider.
# - defaults to the value in testing's Release file (if it is present).
# - Required for the legacy layout.
#ARCHITECTURES     = amd64 arm64 armel armhf i386 mips64el ppc64el s390x

# if you're not in this list, arch: all packages are allowed to break on you
NOBREAKALL_ARCHES = amd64 arm64

# primary architecture used for checking Build-Depends-Indep
ALL_BUILDARCH     = amd64

# if you're in this list, your packages may not stay in sync with the source
OUTOFSYNC_ARCHES  =

# if you're in this list, your uninstallability count may increase
BREAK_ARCHES      =

# if you're in this list, you are a new architecture
NEW_ARCHES        =


## Age policy options
# For the aging policy - all "MINDAYS_X" are measured in days
MINDAYS_LOW       = 10
MINDAYS_MEDIUM    = 5
MINDAYS_HIGH      = 2
MINDAYS_CRITICAL  = 0
MINDAYS_EMERGENCY = 0
# The urgency to assume if none is provided or it is not defined with
# a MINDAYS_$NAME config above
DEFAULT_URGENCY   = medium
# Don't apply penalties (e.g. from autopktest in bounty/penalty mode) for the
# following urgencies
NO_PENALTIES      = high critical emergency
# Lower limit of the age, so accumulated bounties don't let package migrate
# too quick (urgency still has precedent of course)
# Can be given an urgency name
#BOUNTY_MIN_AGE    = high
BOUNTY_MIN_AGE    = 2


## Hint options
# Directory where hints files are stored
HINTSDIR = /path/to/britney/hints-dir

# hint permissions
# Each "HINTS_NAME" defines the list of permissions for a given hints
# file in HINTSDIR.  The name of the hints file should be in all
# lowercase (i.e. "HINTS_FOO" applies to the file "foo" in HINTSDIR)
#
#
#HINTS_USER1         = STANDARD
#HINTS_USER2         = STANDARD force
#HINTS_USER3         = STANDARD force force-hint
#HINTS_USER4         = ALL
#
# Possible "role" hints
#
#HINTS_FREEZE        = block block-all block-udeb
#HINTS_AUTO-REMOVALS = remove


# support for old libraries in testing (smooth update)
#
# This option makes it easier for Britney to handle transitions as she
# will be allowed to keep old binaries around if they have reverse
# dependencies.
#
# This requires that your archive tool is capable of handling binaries
# without a source (or multiple versions of the source package in a
# given suite).  The "old" source will *not* be listed in the output
# for binaries that are retained like this.  DAK is known to handle
# this and will keep the "old" source in the target distribution as
# long as it has binaries left.
#
# - leave it blank to disable it
# - use ALL to enable smooth updates for all the sections
# - naming a non-existent section will effectively disable new
#   smooth updates but still allow removals to occur
SMOOTH_UPDATES    = libs oldlibs


# Whether old binaries in the source distribution should be
# considered as a blocker for migration (ignored by default).
#IGNORE_CRUFT      = 0


# Complete URL to find a specific build log, used for links in the excuses
# Supported placeholders are: arch, suite, source (ie. source package), version
#BUILD_URL         = https://buildd.example.com/status/logs.php?suite=sid&arch={arch}&pkg={source}&ver={version}


## Autopkgtest policy options
# Enable the autopkgtest policy (enabled by default)
#ADT_ENABLE        = no
# Define on which architectures tests should be executed and taken into account
ADT_ARCHES        = amd64
# URI for triggering the autopkgtests. Supported protocols: amqp and file. In
# case the file protocal is used, some mechanism outside britney needs to
# submit the requests.
#ADT_AMQP          = amqp://test_request:password@127.0.0.1
ADT_AMQP          = file:///path/to/britney/debci.input
# space separate list of PPAs to add for test requests and for polling results;
# the *last* one determines the swift container name
#ADT_PPAS          =
# set this to the path of a (r/o) autopkgtest-results.cache for running many parallel
# britney instances for PPAs without updating the cache
#ADT_SHARED_RESULTS_CACHE =
# Swift base URL with the results (must be publicly readable and browsable)
# or file location if results are pre-fetched
#ADT_SWIFT_URL     = https://example.com/some/url
ADT_SWIFT_URL     = file:///path/to/britney/state/debci.json
# Base URL for autopkgtest site, used for links in the excuses (link format
# depends on ADT_AMQP protocol)
ADT_CI_URL        = https://example.com/
# URL for autopkgtest retries, {run_id}, {release}, {arch}, {package} and
# {trigger} will be replaced. {ppas} is replaced as well, but its constructed
# from ADT_PPAS and prepended with '&' (to allow sharing with configurations
# that don't have ADT_PPAS)
#ADT_RETRY_URL     = https://example.com/r?release={release}&arch={arch}&package={package}&trigger={trigger}{ppas}
# URL for autopkgtest logs, {run_id}, {release}, {arch}, {package} and
# {trigger} will be replaced.
#ADT_LOG_URL       = https://example.com/packages/{hash}/{package}/{release}/{arch}/{run_id}/
# Enable the huge queue for packages that trigger vast amounts of tests
# to not starve the regular queue
#ADT_HUGE          = 20
# Autopkgtest results can be used to influence the aging. Not declaring
# ADT_REGRESSION_PENALTY, leaving it empty or setting it to 0 will cause
# regressions to block migration. Use negative numbers if neither penalty nor
# block is desired.
#ADT_REGRESSION_PENALTY = 0
#ADT_SUCCESS_BOUNTY     = 0
# By default, this policy looks at the complete history of autopkgtest
# results. With this option set to 'reference', for failing tests a 'pure' run
# with the trigger 'migration-reference/0' is triggered and used to determine
# if a failure is a regression.
#ADT_BASELINE           = reference
# Britney can retry failures. Specify the time in days after which a retry
# should be triggered.
#ADT_RETRY_OLDER_THAN   = 1
# With ADT_BASELINE = reference, the reference can be refreshed after some
# time. This option specifies the age in days after which a reference for a
# failing test will be retried.
#ADT_REFERENCE_MAX_AGE  = 7
# Depending on the infrastructure, it's possible that a pending test is never
# run. This option specifies the age in days after which a pending test is
# retried. Defaults to 5 days.
#ADT_PENDING_MAX_AGE  = 5
# By default, old results don't expire, which means that the cache can keep on
# growing. This option specifies the age in days after which old results are
# removed from the cache. It's unexpected to have a lower or equal value here
# compared to ADT_REFERENCE_MAX_AGE and ADT_RETRY_OLDER_THAN as that would
# create a window where no results are known at all.
#ADT_RESULTS_CACHE_AGE = 45
# Enable the migration of packages that are missing in testing, and
# whose tests fail (disabled by default)
#ADT_IGNORE_FAILURE_FOR_NEW_TESTS = true


## Reproducible builds policy options
# Enables the policy (disabled by default)
#REPRO_ENABLE           = yes
# Define which architectures to use for the policy
REPRO_ARCHES           = amd64 arm64 armhf i386
# Define which archive components should be checked. Not declaring it or
# leaving it empty means all components are checked.
# (Technically this currenty matches the section of source packages before "/";
# lacking the slash the component is called main (like in Debian).)
#REPRO_COMPONENTS       = main
# Reproducibility results can be used to influence the aging. Not declaring
# REPRO_REGRESSION_PENALTY, leaving it empty or setting it to 0 will cause
# regressions to block migration. Use negative numbers if neither penalty nor
# block is desired.
#REPRO_SUCCESS_BOUNTY   = 0
#REPRO_REGRESSION_PENALTY = 0
# URL for results, {arch} and {package} will be replaced
#REPRO_URL              = https://reproducible-builds.example.com/unstable/{arch}/{package}.html
# URL for retries, {arch} and {package} will be replaced
#REPRO_RETRY_URL        = https://reproducible-builds.example.com/cgi-bin/schedule?suite=unstable&architecture={arch}&pkg={package}


## Other policies
# Enforce Built-Using relationships are satisfied in the target suite (enabled
# by default)
#BUILT_USING_POLICY_ENABLE = no

# Enable the piuparts policy (enabled by default)
#PIUPARTS_ENABLE = no

# Enable the age policy (enabled by default)
#AGE_ENABLE = no

# Enable the rc-bug policy (enabled by default)
#RCBUG_ENABLE = no

# Enable the built-on-buildd policy which blocks sources with uploader built
# binaries(disabled by default)
#CHECK_BUILDD      = yes
